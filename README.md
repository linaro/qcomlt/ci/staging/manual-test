# manual-test



## Getting started

Push a clo/main forked branch with the following files:
- Image.gz
- dtbs.tar.xz
- modules.tar.xz

Those can be obtained with:

```
Create a branch name with your username:

git clone https://git.codelinaro.org/linaro/qcomlt/ci/staging/manual-test
cd manual-test
git checkout origin/clo/main -b myusername/test-vx.x.x
```

```
cd /path/to/kernel
export PATH=$HOME/tools/arm-gnu-toolchain-13.2.Rel1-x86_64-aarch64-none-linux-gnu/bin:$PATH

make ARCH=arm64 CROSS_COMPILE=aarch64-none-linux-gnu- O=build defconfig
make ARCH=arm64 CROSS_COMPILE=aarch64-none-linux-gnu- O=build dtbs Image.gz
make ARCH=arm64 CROSS_COMPILE=aarch64-none-linux-gnu- O=build modules modules_install INSTALL_MOD_PATH=./modules

cp build/arch/arm64/boot/Image.gz /path/to/manual-test/

(mkdir build/dtbs; cd build/arch/arm64/boot/dts; cp --parent */*.dtb ../../../../dtbs)
(cd build; tar cfJ ../dtbs.tar.xz dtbs)
cp dtbs.tar.xz /path/to/repo/

(cd build/modules/; tar cfJ ../../modules.tar.xz ./)
cp modules.tar.xz /path/to/repo/

rm -fr build/modules modules.tar.xz build/dtbs dtbs.tar.xz
```

then:

```
cd /path/to/manual-test/
git add Image.gz dtbs.tar.xz modules.tar.xz
git commit -a -s -m "Test Kernel build"
git push origin myusername/test-vx.x.x
```

or with GIT LFS:
```
git lfs track Image.gz dtbs.tar.xz modules.tar.xz
git add .gitattributes
git add Image.gz dtbs.tar.xz modules.tar.xz
git commit -a -s -m "Test Kernel build"
git push origin myusername/test-vx.x.x
```

When test has finished, you can delete the branch or continue pushing over it. Rebasing/force push is allowed.